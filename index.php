<?php

/**
 * @file
 * Index file for movie booking website.
 */

session_start();
require_once 'model.php';
require_once 'controller.php';
$controller = new Controller();
$model = new Model();
$uri = $_SERVER['PHP_SELF'];
if ($uri == '/index.php/admin') {
  $controller->adminLogin();
}
elseif ($uri == '/index.php/adminverify') {
  $controller->adminVerify();
  if ($_SESSION["admin_id"] && $_SESSION["admin_name"]) {
    require_once 'admin/home.php';
  }
  else {
    $controller->adminLogin();
  }
}
elseif ($uri == '/index.php/viewbookings') {
  if ($_SESSION["admin_id"] && $_SESSION["admin_name"]) {
    $controller->viewList();
  }
  else {
    $controller->adminLogin();
  }
}
elseif ($uri == '/index.php/addTheatre') {
  if ($_SESSION["admin_id"] && $_SESSION["admin_name"]) {
    $controller->addTheatre();
  }
  else {
    $controller->adminLogin();
  }
}
elseif ($uri == '/index.php/updatemovie') {
  if ($_SESSION["admin_id"] && $_SESSION["admin_name"]) {
    $controller->updateMovie();
  }
  else {
    $controller->adminLogin();
  }
}
elseif ($uri == '/index.php/logout') {
  session_destroy();
  session_unset();
  header("http:bms.com/index.php/admin");
}
elseif ($uri == '/index.php') {
  $controller->listMovies();
}
elseif ($uri == '/index.php/book') {
  $controller->listTheatres();
}
elseif ($uri == '/index.php/enter') {
  require_once 'user/enter.php';
}
elseif ($uri == '/index.php/bookticket') {
  $controller->bookTickets();
}
elseif ($uri == '/index.php/fetchtheatre') {
  $model->fetchTheatre();
}
elseif ($uri == '/index.php/fetchscreen') {
  $model->fetchScreen();
}
elseif ($uri == '/index.php/fetchmovies') {
  $model->fetchMovie();
}
else {
  echo $uri;
}
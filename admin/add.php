<?php
/**
 * @file
 * File to add new theatres.
 */
?>
<form action="" method="post">
  <label for="theatre">Name of Theatre</label>
  <input type="text" name="theatre" id="" required>
  <br>
  <label for="location">Location of Theatre</label>
  <input type="text" name="location" id="" required>
  <br>
  <button onclick="adds()">Add screen</button>
  <div id= "addscreen">
    <label for="screen[1]">Screen 1:</label>
    <input type="text" name="screen[1]" id="" required>
    <br>
    <label for="seats[1]">Seat capacity :</label>
    <input type="text" name="seats[1]" id="" required>
    <br>
    <label for="charge[1]">Ticket Charge :</label>
    <input type="text" name="charge[1]" id="" required>
    <br>
  </div>
  <input type="submit" value="Add Theatre">
</form>
<script>
var i = 2;
function adds() {
  document.getElementById("addscreen").innerHTML += "<label for=\"screen[1]\">Screen " + i + ":</label><input type=\"text\" name=\"screen["+i+"]\" required><br><label for=\"seats["+i+"]\">Seat capacity :</label><input type=\"text\" name=\"seats["+i+"]\" required><br><label for=\"charge["+i+"]\">Ticket Charge :</label><input type=\"text\" name=\"charge["+i+"]\" required><br>";
  i++;
}
</script>
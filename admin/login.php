<?php

/**
 * @file
 * Admin Login form.
 */
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title> BMS</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSS-->
    <link rel="stylesheet" type="text/css" href="/css/main.css">
    <link rel="stylesheet" type="text/css" href="/js/calendar/css/pignose.calendar.css">
    <script src="/js/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="/js/plugins/select2.min.js"></script>
    <link href = "/css/style.css" rel = "stylesheet">
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/bootstrap-datepicker3.min.css" rel="stylesheet">
    <script src="/js/jquery-3.1.1.js"></script>
    <script src="/js/jquery-3.1.1.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/plugins/sweetalert.min.js"></script>
    <script src="/js/sweetalert-dev.js"></script>
    <script src="/js/bootstrap-datepicker.min.js"></script>
    <style>
      /* Remove the navbar's default margin-bottom and rounded borders */ 
      .navbar {
      margin-bottom: 0;
      border-radius: 0;
      }
      .navbar-brand {
      float: left;
      height: 50px;
      padding: 15px 15px;
      font-size: 18px;
      line-height: 20px;
      position: relative;
      left: 455px;
      }
      .abcRioButton.abcRioButtonLightBlue { margin: 0 auto;}
      /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
      .row.content {height: 450px}
      /* Set gray background color and 100% height */
      .sidenav {
      padding-top: 20px;
      background-color: #f1f1f1;
      height: 100%;
      }
      /* Set black background color, white text and some padding */
      footer {
      background-color: #555;
      color: white;
      padding: 15px;
      }
      /* On small screens, set height to 'auto' for sidenav and grid */
      @media screen and (max-width: 767px) {
      .sidenav {
      height: auto;
      padding: 15px;
      }
      .row.content {height:auto;} 
      }
    </style>
  </head>
  <body>
    <nav class="navbar navbar-inverse">
      <div class="container-fluid">
        <div class="navbar-header">
          <a class="navbar-brand" href="#">BookMyShow</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
        </div>
      </div>
    </nav>
    <div class="container ">
      <div class="container-fluid center_div "></div>
      <div class="col-md-4 column"></div>
      <div class="col-md-5 column">
        <div id="a" style="position:relative; top:150px;">
          <form  action="http://bms.com/index.php/adminverify" id="form" method="post" align="center">
            <table id="table" class="table center-table table-inverse table-hover table-striped text-centered" cellspacing="0" align="center">
              <thead>
                <tr>
                  <th width="50">Username</th>
                  <th width="300">
                    <input name="name" id="name" placeholder="Username"  class="form-control" type="text" required>
                  </th>
                </tr>
                <tr>
                  <th>Password</th>
                  <th width="300">
                    <input name="pwd" id="pwd" placeholder="Password"  class="form-control" type="password" required>
                  </th>
                </tr>
                <tr>
                  <th >	</th>
                  <th><input type="submit" class="btn" name="submit" id="submit" value="SUBMIT" /></th>
                </tr>
              </thead>
            </table>
          </form>
        </div>
      </div>
    </div>
  </body>
</html>

<?php

/**
 * @file
 * File to set movies to screens.
 */
?>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<div>
<form action="updatemovie" method="post">
  Theatre :
  <select name="theatre" id="theatre" onchange="addscreen()">
  </select>
  <br>
  Screen:
  <select name="screen" id="screen" onchange="addmovies()">
  </select>
  <br>
  Movie:
  <select name="movies" id="movies" onchange="addtime()">
  </select>
  <br>
  <div id="time">
  </div>
  <input type="submit" value="Update Movie">
</form>
</div>

<script>
$.ajax({
  url : 'fetchtheatre',
  success: function(response){
    document.getElementById("theatre").innerHTML = response;
  }
});

function addscreen() {
  $.ajax({
    url : 'fetchscreen',
    type: 'POST',
    data: 'theatre_id='+document.getElementById("theatre").value,
    success: function(response){
      document.getElementById("screen").innerHTML = response;
    }
  });
}

function addmovies() {
  $.ajax({
    url : 'fetchmovies',
    success: function(response){
      document.getElementById("movies").innerHTML = response;
    }
  });
}
function addtime(){
  document.getElementById("time").innerHTML = "<label for=time>Show time:</label><input type=time name=time><br/><label for=date>Date:</label><input type=date name=date>";
}
</script>
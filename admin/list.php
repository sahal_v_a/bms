<?php

/**
 * @file
 * File to display the list of tickets booked.
 */
?>
<body id="row">
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<span style="float:right">
Enter booking Id:
<input style="width:50px" type="text" name="booking_id" id="bid" value="<?php echo $_POST['bid']?>">
<br>
<button onclick="search()">Search</button>
</span>
<table>
  <thead>
    <th>Booking Id</th>
    <th>Movie</th>
    <th>Theatre</th>
    <th>Screen</th>
    <th>Show time</th>
    <th>Booked by</th>
    <th>Email</th>
    <th>Phone</th>
    <th>No.of.Seats</th>
  </thead>
  <?php foreach ($list as $value) :?>
  <?php if ($_POST["bid"]) :?>
  <?php if ($_POST["bid"] == $value["booking_id"]) :?>
  <tr id="row">
    <td><?php echo $value["booking_id"]?></td>
    <td><?php echo $value["movie"]?></td>
    <td><?php echo $value["theatre"]?></td>
    <td><?php echo $value["screen"]?></td>
    <td><?php echo $value["show_time"]?></td>
    <td><?php echo $value["name"]?></td>
    <td><?php echo $value["email"]?></td>
    <td><?php echo $value["phone"]?></td>
    <td><?php echo $value["no_of_seats"]?></td>
  </tr>
  <?php endif;?>
  <?php else :?>
  <tr>
    <td><?php echo $value["booking_id"]?></td>
    <td><?php echo $value["movie"]?></td>
    <td><?php echo $value["theatre"]?></td>
    <td><?php echo $value["screen"]?></td>
    <td><?php echo $value["show_time"]?></td>
    <td><?php echo $value["name"]?></td>
    <td><?php echo $value["email"]?></td>
    <td><?php echo $value["phone"]?></td>
    <td><?php echo $value["no_of_seats"]?></td>
  </tr>
  <?php endif;?>
  <?php endforeach;?>
</table>
</body>
<script>
function search() {
  var id = document.getElementById("bid").value;
  $.ajax({
    url: 'viewbookings',
    type: 'POST',
    data: 'bid=' + id ,
    success: function (response) {
      document.getElementById("row").innerHTML = response;
    }
  });
}
</script>
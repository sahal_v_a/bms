<?php

/**
 * @file
 * Controller class for controlling actions of BMS.
 */

require_once 'model.php';

/**
 * Controller class.
 */
class Controller {

  /**
   * Function to display admin login page.
   */
  public function adminLogin() {
    require_once 'admin/login.php';
  }

  /**
   * Function to verify admin.
   */
  public function adminVerify() {
    $model = new Model();
    $username = $_POST["name"];
    $password = sha1($_POST["pwd"]);
    $admin = $model->fetchAdmin($username, $password);
    if ($admin) {
      $_SESSION["admin_id"] = $admin[0]["id"];
      $_SESSION["admin_name"] = $admin[0]["username"];
    }
  }

  /**
   * Function to display the list ot tickets booked.
   */
  public function viewList() {
    $model = new Model();
    $list = $model->fetchList();
    require_once 'admin/list.php';
  }

  /**
   * Function to Add new theatre .
   */
  public function addTheatre() {
    require_once 'admin/add.php';
    $model = new Model();
    $model->addTheatres();
  }

  /**
   * Function to update movie to a theatre .
   */
  public function updateMovie() {
    require_once 'admin/movie.php';
    $model = new Model();
    $model->updateMovies();
  }

  /**
   * Function to display the list ot movies for user.
   */
  public function listMovies() {
    $model = new Model();
    $list = $model->fetchMovies();
    require_once 'user/home.php';
  }

  /**
   * Function to display the list of theatres for booking the given movie for user.
   */
  public function listTheatres() {
    $model = new Model();
    $list = $model->fetchTheatres($_GET["mid"]);
    require_once 'user/book.php';
  }

  /**
   * Function to update booking_details and users.
   */
  public function bookTickets() {
    $model = new Model();
    $ticket = $model->enterBookingDetails();
    require_once 'user/ticketscreen.php';
  }
}
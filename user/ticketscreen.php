<div style="border:solid black 2px;width:500px;margin-top:10%;margin-left:20%">
<center>
  <table>
    <thead colspan="2">
      <th><h3><center>Your Ticket Details</center></h3></th>
    </thead>
    <tr>
      <td>Booking Id :</td>
      <td><?php echo $ticket["booking_id"]?></td>
    </tr>
    <tr>
      <td>Email :</td>
      <td><?php echo $ticket["email"]?></td>
    </tr>
    <tr>
      <td>Phone :</td>
      <td><?php echo $ticket["phone"]?></td>
    </tr>
    <tr>
      <td>Movie :</td>
      <td><?php echo $ticket["movie"]?></td>
    </tr>
    <tr>
      <td>Theatre :</td>
      <td><?php echo $ticket["theatre"]?></td>
    </tr>
    <tr>
      <td>Screen :</td>
      <td><?php echo $ticket["screen"]?></td>
    </tr>
    <tr>
      <td>Show time :</td>
      <td><?php echo $ticket["show_time"]?></td>
    </tr>
    <tr>
      <td>No.of.Seats :</td>
      <td><?php echo $ticket["no_of_seats"]?></td>
    </tr>
    <tr>
      <td>Total Amount :</td>
      <td><?php echo $ticket["total_amount"]?></td>
    </tr>
  </table>
</div>
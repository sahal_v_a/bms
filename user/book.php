<?php

/**
 * @file
 * Theatre listing page for selected movie.
 */
?>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<div style="float:right" id="b">

</div>
<table>
  <thead>
    <th>Sl.No</th>
    <th>Theatre</th>
    <th>Screen</th>
    <th>Show time</th>
    <th>Seats Available</th>
  </thead>
  <?php foreach ($list as $value) :?>
  <tr>
    <td><?php echo ++$i;?></td>
    <td><?php echo $value["theatre"]?></td>
    <td><?php echo $value["screen"]?></td>
    <td><?php echo $value["show_time"]?></td>
    <td><?php echo $value["seats_available"]?></td>
    <td><button onclick="book(<?php echo $value["ms_id"]?>)">Book</button></td>
  </tr>
  <?php endforeach?>
</table>
<script>
function book(msid) {
  $.ajax({
    url: 'enter',
    type: 'POST',
    data:'msid=' + msid,
    success: function(response){
      document.getElementById("b").innerHTML = response;
    }
  });
}
</script>
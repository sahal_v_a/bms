<?php

/**
 * @file
 * User homepage listing movies.
 */
?>
<table>
  <thead>
    <th>Sl.No</th>
    <th>Movie</th>
    <th>Language</th>
  </thead>
  <?php foreach ($list as $value) :?>
  <tr>
    <td><?php echo ++$i;?></td>
    <td>
      <a href="/index.php/book?mid=<?php echo $value['mid']?>"><?php echo $value["movie"]?></a>
    </td>
    <td><?php echo $value["language"]?></td>
  </tr>
  <?php endforeach?>
</table>
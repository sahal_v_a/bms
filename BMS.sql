-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 31, 2018 at 11:00 AM
-- Server version: 5.7.21-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `BMS`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`) VALUES
(1, 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997');

-- --------------------------------------------------------

--
-- Table structure for table `booking_details`
--

CREATE TABLE `booking_details` (
  `id` int(11) NOT NULL,
  `ms_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `booked_seats` int(11) NOT NULL,
  `total` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booking_details`
--

INSERT INTO `booking_details` (`id`, `ms_id`, `user_id`, `booked_seats`, `total`) VALUES
(3, 1, 9, 10, 1000),
(4, 1, 10, 10, 1000),
(5, 1, 11, 10, 1000),
(6, 1, 12, 10, 1000),
(7, 1, 13, 10, 1000),
(8, 1, 14, 10, 1000),
(9, 1, 15, 10, 1000),
(10, 1, 16, 10, 1000),
(11, 1, 17, 10, 1000),
(12, 1, 18, 10, 1000),
(13, 1, 19, 10, 1000),
(14, 1, 20, 10, 1000),
(15, 1, 21, 10, 1000),
(16, 1, 22, 10, 1000),
(17, 1, 23, 10, 1000),
(18, 2, 24, 5, 750);

-- --------------------------------------------------------

--
-- Table structure for table `movies`
--

CREATE TABLE `movies` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `language` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `movies`
--

INSERT INTO `movies` (`id`, `title`, `language`) VALUES
(1, 'Bahubali 1', 'Tamil'),
(2, 'Bahubali 2', 'Telugu');

-- --------------------------------------------------------

--
-- Table structure for table `movies_in_screen`
--

CREATE TABLE `movies_in_screen` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `screen_id` int(11) NOT NULL,
  `movie_id` int(11) NOT NULL,
  `booked_seats` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `movies_in_screen`
--

INSERT INTO `movies_in_screen` (`id`, `date`, `time`, `screen_id`, `movie_id`, `booked_seats`) VALUES
(2, '2018-01-01', '00:00:00', 2, 2, 0),
(4, '2018-01-01', '00:00:00', 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `screens`
--

CREATE TABLE `screens` (
  `id` int(11) NOT NULL,
  `theatre_id` int(11) NOT NULL,
  `total_seats` int(11) NOT NULL,
  `ticket_charge` int(11) NOT NULL,
  `screen_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `screens`
--

INSERT INTO `screens` (`id`, `theatre_id`, `total_seats`, `ticket_charge`, `screen_name`) VALUES
(1, 1, 50, 100, 'screen1'),
(2, 1, 100, 150, 'screen2'),
(3, 2, 50, 50, 'screen1'),
(4, 2, 100, 100, 'screen2'),
(5, 2, 150, 150, 'screen3'),
(6, 3, 50, 50, 'screen1'),
(7, 3, 100, 100, 'screen2'),
(8, 3, 150, 150, 'screen3');

-- --------------------------------------------------------

--
-- Table structure for table `theatre`
--

CREATE TABLE `theatre` (
  `id` int(11) NOT NULL,
  `location` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `theatre`
--

INSERT INTO `theatre` (`id`, `location`, `name`) VALUES
(1, 'Trivandrum', 'Krishna theatre'),
(2, 'trivndrm', 'new theatre'),
(3, 'trivndrum', 'new theatre');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` bigint(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `phone`) VALUES
(1, 'sahal.email.com', 9809447393),
(2, 'karun@email.com', 1234567891),
(3, 'dsa@sd', 123),
(4, 'dsa@sd', 123),
(5, 'sahal@email.com', 123),
(6, 'sahal@email.com', 123),
(7, 'sahal@email.com', 123),
(8, 'sahal@email.com', 12345),
(9, 'dsa@sd', 123),
(10, 'dsa@sd', 123),
(11, 'dsa@sd', 123),
(12, 'dsa@sd', 123),
(13, 'dsa@sd', 123),
(14, 'dsa@sd', 123),
(15, 'dsa@sd', 123),
(16, 'dsa@sd', 123),
(17, 'dsa@sd', 123),
(18, 'dsa@sd', 123),
(19, 'dsa@sd', 123),
(20, 'dsa@sd', 123),
(21, 'dsa@sd', 123),
(22, 'dsa@sd', 123),
(23, 'dsa@sd', 123),
(24, 'asas@gmail.com', 123);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `booking_details`
--
ALTER TABLE `booking_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `movies`
--
ALTER TABLE `movies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `movies_in_screen`
--
ALTER TABLE `movies_in_screen`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `screens`
--
ALTER TABLE `screens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `theatre`
--
ALTER TABLE `theatre`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `booking_details`
--
ALTER TABLE `booking_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `movies`
--
ALTER TABLE `movies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `movies_in_screen`
--
ALTER TABLE `movies_in_screen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `screens`
--
ALTER TABLE `screens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `theatre`
--
ALTER TABLE `theatre`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

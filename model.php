<?php

/**
 * @file
 * Model file for BMS.
 */

/**
 * Class model.
 */
class Model {

  /**
   * Function to open database connection.
   */
  public function openDatabaseConnection() {
    $server = "localhost";
    $user = "root";
    $pass = "root";
    $dbname = "BMS";
    $connection = new PDO("mysql:host=$server;dbname=$dbname", $user, $pass);
    return $connection;
  }

  /**
   * Function to fetch admin login details.
   */
  public function fetchAdmin($username, $password) {
    $link = $this->openDatabaseConnection();
    $stmt = $link->prepare("SELECT * FROM admin WHERE username = :u AND password = :p");
    $stmt->bindParam(":u", $username);
    $stmt->bindParam(":p", $password);
    $f = $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    return $result;
  }

  /**
   * Function to fetch the ticket list.
   */
  public function fetchList() {
    $link = $this->openDatabaseConnection();
    $stmt = $link->prepare(
      "SELECT u.email AS email,
      u.phone AS phone,
      u.name AS name,
      b.id AS booking_id,
      b.booked_seats AS no_of_seats,
      b.total AS total_amount,
      ms.time AS show_time,
      m.title AS movie,
      s.screen_name AS screen,
      t.location AS location,
      t.name AS theatre
      FROM users u
      JOIN booking_details b
      ON u.id = b.user_id
      JOIN movies_in_screen ms
      ON b.ms_id = ms.id
      JOIN movies m
      ON ms.movie_id = m.id
      JOIN screens s
      ON ms.screen_id = s.id
      JOIN theatre t 
      ON s.theatre_id = t.id"
    );
    $f = $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    return $result;
  }

  /**
   * Function to fetch movies list.
   */
  public function fetchMovies() {
    $link = $this->openDatabaseConnection();
    $stmt = $link->prepare(
      "SELECT ms.time AS show_time,
      m.id as mid,
      m.title AS movie,
      m.language,
      s.screen_name AS screen,
      t.location AS location,
      t.name AS theatre
      FROM movies_in_screen ms
      JOIN movies m
      ON ms.movie_id = m.id
      JOIN screens s
      ON ms.screen_id = s.id
      JOIN theatre t 
      ON s.theatre_id = t.id"
    );
    $f = $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    return $result;
  }

  /**
   * Function to fetch Theatres list playing the given movie.
   */
  public function fetchtheatres($mid) {
    $link = $this->openDatabaseConnection();
    $stmt = $link->prepare(
      "SELECT ms.time AS show_time,
      m.id as mid,
      ms.id as ms_id,
      s.ticket_charge AS charge,
      m.title AS movie,
      m.language,
      s.screen_name AS screen,
      t.location AS location,
      t.name AS theatre,
      (s.total_seats-ms.booked_seats) AS seats_available
      FROM movies_in_screen ms
      JOIN movies m
      ON ms.movie_id = m.id
      JOIN screens s
      ON ms.screen_id = s.id
      JOIN theatre t 
      ON s.theatre_id = t.id
      WHERE m.id = :i"
    );
    $stmt->bindParam(":i", $mid);
    $f = $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    return $result;
  }

  /**
   * Function to enter booking_details and users.
   */
  public function enterBookingDetails() {
    $email = htmlentities($_POST["email"], ENT_QUOTES);
    $phone = htmlentities($_POST["phone"], ENT_QUOTES);
    $nos = htmlentities($_POST["no_of_seats"], ENT_QUOTES);
    $link = $this->openDatabaseConnection();
    $stmt = $link->prepare(
      "INSERT INTO users(email, phone)
      VALUES (:e, :p)"
    );
    $stmt->bindParam(":e", $email);
    $stmt->bindParam(":p", $phone);
    $stmt->execute();
    $uid = $link->lastInsertId();
    $stmt = $link->prepare(
      "SELECT ticket_charge 
      FROM movies_in_screen ms
      JOIN screens s
      ON s.id = ms.screen_id
    WHERE ms.id = {$_POST['ms_id']}"
    );
    $stmt->execute();
    $charge = $stmt->fetch(PDO::FETCH_ASSOC);
    $total = $nos * $charge["ticket_charge"];
    $stmt = $link->prepare(
      "INSERT INTO booking_details(ms_id, user_id, booked_seats, total)
      VALUES(:m, :u, :bs, $total)"
    );
    $stmt->bindParam(":m", $_POST["ms_id"]);
    $stmt->bindParam(":u", $uid);
    $stmt->bindParam(":bs", $nos);
    $stmt->execute();
    $stmt = $link->prepare(
      "UPDATE movies_in_screen 
      SET booked_seats = booked_seats+$nos
      WHERE id = {$_POST['ms_id']} "
    );
    $stmt->execute();
    $stmt = $link->prepare(
      "SELECT u.email AS email,
      u.phone AS phone,
      b.id AS booking_id,
      b.booked_seats AS no_of_seats,
      b.total AS total_amount,
      ms.date AS show_date,
      ms.time AS show_time,
      m.title AS movie,
      s.screen_name AS screen,
      t.location AS location,
      t.name AS theatre
      FROM users u
      JOIN booking_details b
      ON u.id = b.user_id
      JOIN movies_in_screen ms
      ON b.ms_id = ms.id
      JOIN movies m
      ON ms.movie_id = m.id
      JOIN screens s
      ON ms.screen_id = s.id
      JOIN theatre t 
      ON s.theatre_id = t.id
      WHERE u.id = $uid"
    );
    $f = $stmt->execute();
    $result = $stmt->fetch(PDO::FETCH_ASSOC);
    return $result;
  }

  /**
   * Function to add theatre to database.
   */
  public function addTheatres() {
    $link = $this->openDatabaseConnection();
    $stmt = $link->prepare(
      "INSERT INTO theatre(location, name)
      VALUES(:l, :n)"
    );
    $location = htmlentities($_POST["location"], ENT_QUOTES);
    $name = htmlentities($_POST["theatre"], ENT_QUOTES);
    $stmt->bindParam(":l", $location);
    $stmt->bindParam(":n", $name);
    $stmt->execute();
    $tid = $link->lastInsertId();
    $stmt = $link->prepare(
      "INSERT INTO screens(theatre_id, total_seats, ticket_charge, screen_name)
      VALUES(:tid, :ts, :tc, :sn)"
    );
    $i = 1;
    while ($_POST["screen"][$i]) {
      $stmt->bindParam(":tid", $tid);
      $seats = htmlentities($_POST["seats"][$i], ENT_QUOTES);
      $stmt->bindParam(":ts", $seats);
      $charge = htmlentities($_POST["charge"][$i], ENT_QUOTES);
      $stmt->bindParam(":tc", $charge);
      $name = htmlentities($_POST["screen"][$i++], ENT_QUOTES);
      $stmt->bindParam(":sn", $name);
      $stmt->execute();
    }
  }

  /**
   * Function to fetch all theatres.
   */
  public function fetchTheatre() {
    $link = $this->openDatabaseConnection();
    $stmt = $link->prepare(
      "SELECT * 
      FROM theatre"
    );
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $string = '<option>Select Theatre</option>';
    foreach ($result as $theatre) {
      $string .= "<option value = {$theatre['id']}>" . $theatre["name"] . ", " . $theatre["location"] . "</option>";
    }
    echo $string;
  }

  /**
   * Function to fetch all screens of that theatre.
   */
  public function fetchScreen() {
    $link = $this->openDatabaseConnection();
    $stmt = $link->prepare(
      "SELECT id,screen_name 
      FROM screens 
      WHERE theatre_id = {$_POST['theatre_id']}"
    );
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $string = '<option>Select Screen</option>';
    foreach ($result as $screen) {
      $string .= "<option value = {$screen['id']}>" . $screen["screen_name"] . "</option>";
    }
    echo $string;
  }

  /**
   * Function to fetch all movies.
   */
  public function fetchMovie() {
    $link = $this->openDatabaseConnection();
    $stmt = $link->prepare(
      "SELECT * 
      FROM movies"
    );
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $string = '<option>Select Movie</option>';
    foreach ($result as $movie) {
      $string .= "<option value = {$movie['id']}>" . $movie["title"] . ", " . $movie["language"] . "</option>";
    }
    echo $string;
  }

  /**
   * Function to set movies to a particular screen.
   */
  public function updateMovies() {
    $link = $this->openDatabaseConnection();
    $tid = $_POST["theatre"];
    $sid = $_POST["screen"];
    $mid = $_POST["movies"];
    $time = $_POST["time"];
    $date = $_POST["date"];
    $stmt = $link->prepare(
      "SELECT id FROM movies_in_screen WHERE id = $sid"
    );
    $stmt->execute();
    $f = $stmt->fetch(PDO::FETCH_ASSOC);
    $stmt = $link->prepare(
      "UPDATE movies_in_screen 
      SET date = :d, 
      time = :t, 
      movie_id = :m, 
      booked_seats = 0
      WHERE id = :s"
    );
    $stmt->bindParam(":d", $date);
    $stmt->bindParam(":t", $time);
    $stmt->bindParam(":s", $sid);
    $stmt->bindParam(":m", $mid);
    $stmt->execute();
    if (!$f) {
      $stmt = $link->prepare(
        "INSERT INTO movies_in_screen(date, time, screen_id, movie_id, booked_seats)
        VALUES(:d , :t, :s, :m, 0)"
      );
      $stmt->bindParam(":d", $date);
      $stmt->bindParam(":t", $time);
      $stmt->bindParam(":s", $sid);
      $stmt->bindParam(":m", $mid);
      $stmt->execute();
    }

  }

}
